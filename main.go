package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

var l = logrus.New()

func main() {

	l.Info("Billing server is starting...")

	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		l.Infof("Received http request on /test from %s", r.RemoteAddr)
		fmt.Fprintf(w, "%s", "Test ok")
	})
	http.HandleFunc("/lastbill", func(w http.ResponseWriter, r *http.Request) {
		var host, port, user, pwd string
		host = os.Getenv("DB_HOST")
		port = os.Getenv("DB_PORT")
		user = os.Getenv("DB_USER")
		pwd = os.Getenv("DB_PASSWORD")
		if host != "" && port != "" && user != "" && pwd != "" {
			//TODO actually connect to a real database
			l.Infof("Connected to database %s:%s as %s:%s", host, port, user, pwd)
			fmt.Fprintf(w, "%s", "Test ok")
		} else {
			l.Info("Unable to connect to the database")
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "%s", "Not connected")
		}
	})
	l.Fatal(http.ListenAndServe(":8090", nil))
}
