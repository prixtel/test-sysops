FROM golang:1.16 AS builder
WORKDIR /app/builder

COPY . /app/builder
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app/builder/main .
CMD ["./main"]  
